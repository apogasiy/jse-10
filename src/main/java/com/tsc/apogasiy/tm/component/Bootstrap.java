package main.java.com.tsc.apogasiy.tm.component;

import main.java.com.tsc.apogasiy.tm.api.controller.ICommandController;
import main.java.com.tsc.apogasiy.tm.api.controller.IProjectController;
import main.java.com.tsc.apogasiy.tm.api.controller.ITaskController;
import main.java.com.tsc.apogasiy.tm.api.repository.ICommandRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.api.service.ICommandService;
import main.java.com.tsc.apogasiy.tm.api.service.IProjectService;
import main.java.com.tsc.apogasiy.tm.api.service.ITaskService;
import main.java.com.tsc.apogasiy.tm.constant.ArgumentConst;
import main.java.com.tsc.apogasiy.tm.constant.TerminalConst;
import main.java.com.tsc.apogasiy.tm.controller.CommandController;
import main.java.com.tsc.apogasiy.tm.controller.ProjectController;
import main.java.com.tsc.apogasiy.tm.controller.TaskController;
import main.java.com.tsc.apogasiy.tm.repository.CommandRepository;
import main.java.com.tsc.apogasiy.tm.repository.ProjectRepository;
import main.java.com.tsc.apogasiy.tm.repository.TaskRepository;
import main.java.com.tsc.apogasiy.tm.service.CommandService;
import main.java.com.tsc.apogasiy.tm.service.ProjectService;
import main.java.com.tsc.apogasiy.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    public Bootstrap() {
    }

    public void start(final String[] args) {
        commandController.showWelcome();
        parseArgs(args);
        process();
    }

    public void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            parseCommand(command);
            System.out.println();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            default:
                commandController.showError();
                break;
        }
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showError();
                break;
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        for (String param : args)
            parseArg(param);
    }
}
