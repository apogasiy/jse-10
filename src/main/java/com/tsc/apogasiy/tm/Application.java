package main.java.com.tsc.apogasiy.tm;

import main.java.com.tsc.apogasiy.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
