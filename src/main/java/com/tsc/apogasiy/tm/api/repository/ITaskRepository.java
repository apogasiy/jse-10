package main.java.com.tsc.apogasiy.tm.api.repository;

import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
