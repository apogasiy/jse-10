package main.java.com.tsc.apogasiy.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

}
