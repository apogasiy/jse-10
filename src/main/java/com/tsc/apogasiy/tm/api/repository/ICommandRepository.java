package main.java.com.tsc.apogasiy.tm.api.repository;

import main.java.com.tsc.apogasiy.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
